﻿using UnityEngine;
using UnityEngine.UI;

public class CharacterSheetController : MonoBehaviour
{
    public PlayerStatsManager playerStats;

    public Text attributePointsDisplay;

    public Text strengthDisplay;
    public Text dexterityDisplay;
    public Text intelligenceDisplay;
    public Text enduranceDisplay;

    public Text healthDisplay;
    public Text maxHealthDisplay;
    public Text healthRegenDisplay;

    public Text damageDisplay;
    public Text attackSpeedDisplay;
    public Text rangeDisplay;

    public Text physicalResistanceDisplay;
    public Text fireResistanceDisplay;
    public Text lightningResistanceDisplay;
    public Text poisonResistanceDisplay;

    public Text experiencePointsDisplay;
    public Text levelDisplay;

    [SerializeField]
    private int attributePoints = 3;

	// Use this for initialization
	void Start () {
        playerStats = FindObjectOfType<PlayerStatsManager>();
        playerStats.LevelUpEvent += OnLevelUp;

        attributePointsDisplay.text = "" + attributePoints;
        strengthDisplay.text = "" + playerStats.strength;
        dexterityDisplay.text = "" + playerStats.dexterity;
        intelligenceDisplay.text = "" + playerStats.intelligence;
        enduranceDisplay.text = "" + playerStats.endurance;

        UpdateDisplay();
        UpdateButtons();
    }

    void Update()
    {
        UpdateDisplay();
    }

    public void OnLevelUp()
    {
        attributePoints += 3;
        UpdateButtons();
    }

    public void AddStrengthPoint()
    {
        playerStats.AddStrengthPoint();
        strengthDisplay.text = "" + playerStats.strength;
        attributePoints--;
        UpdateButtons();
    }

    public void AddDexterityPoint()
    {
        playerStats.AddDexterityPoint();
        dexterityDisplay.text = "" + playerStats.dexterity;
        attributePoints--;
        UpdateButtons();
    }

    public void AddIntelligencePoint()
    {
        playerStats.AddIntelligencePoint();
        intelligenceDisplay.text = "" + playerStats.intelligence;
        attributePoints--;
        UpdateButtons();
    }

    public void AddEndurancePoint()
    {
        playerStats.AddEndurancePoint();
        enduranceDisplay.text = "" + playerStats.endurance;
        attributePoints--;
        UpdateButtons();
    }

    private void UpdateButtons()
    {
        attributePointsDisplay.text = "" + attributePoints;
        if (attributePoints > 0)
        {
            foreach (Button button in GetComponentsInChildren<Button>())
            {
                button.interactable = true;
            }
        }
        else
        {
            foreach (Button button in GetComponentsInChildren<Button>())
            {
                button.interactable = false;
            }
        }
    }

    private void UpdateDisplay()
    {
        healthDisplay.text = "" + playerStats.health;
        maxHealthDisplay.text = "" + playerStats.maxHealth;
        healthRegenDisplay.text = "" + playerStats.healthRegen;

        damageDisplay.text = "" + playerStats.damage;
        attackSpeedDisplay.text = "" + playerStats.attackSpeed;
        rangeDisplay.text = "" + playerStats.range;

        physicalResistanceDisplay.text = "" + playerStats.physicalResistance * 100f + "%";
        fireResistanceDisplay.text = "" + playerStats.fireResistance * 100f + "%";
        lightningResistanceDisplay.text = "" + playerStats.lightningResistance * 100f + "%";
        poisonResistanceDisplay.text = "" + playerStats.poisonResistance * 100f + "%";

        experiencePointsDisplay.text = "" + playerStats.experiencePoints;
        levelDisplay.text = "" + playerStats.level;
    }
}
