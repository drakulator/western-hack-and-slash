﻿using UnityEngine;
using UnityEngine.UI;

public class FloatingTextHolder : MonoBehaviour
{
    public GameObject floatingText;
    public Text nameComponent;
    public Text titleComponent;
    public GameObject healthComponent;
}
