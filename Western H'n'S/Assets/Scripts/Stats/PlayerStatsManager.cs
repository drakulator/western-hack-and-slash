﻿using UnityEngine;
using System.Collections.Generic;

public class PlayerStatsManager : StatsManager
{
    [Header("Projectile")]
    [Space]
    [ReadOnly] public GameObject projectile;

    [Header("Name")]
    [Space]
    public string playerName;

    [Header("Attributes")]
    [Space]
    public int strength = 10;
    public int dexterity = 10;
    public int intelligence = 10;
    public int endurance = 10;

    [Space]
    public int experiencePoints;
    public int level = 1;
    [Space]

    [ReadOnly] public bool isAttackEnabled = false;

    [HideInInspector]
    public List<Item> equippedItems;

    private static int[] experienceLevels =
    {
        0,
        100,
        500,
        1000,
        2000,
        5000
    };

    public CharacterSheetController characterSheetController;

    public delegate void LevelUpDelegate();
    public event LevelUpDelegate LevelUpEvent;

    //base stats
    //--------------------------------------------
    [HideInInspector] public int maxHealthBase = 100;
    [HideInInspector] public int healthRegenBase = 1;
    [HideInInspector] public float moveSpeedBase = 10f;
    [HideInInspector] public float angularSpeedBase = 120f;

    [HideInInspector] public float attackSpeedStatBonus = 0f;

    [HideInInspector] public float physicalResistanceBase = 0f;
    [HideInInspector] public float fireResistanceBase = 0f;
    [HideInInspector] public float lightningResistanceBase = 0f;
    [HideInInspector] public float poisonResistanceBase = 0f;

    [HideInInspector] public float weaponModifier1H = 0f;
    [HideInInspector] public float weaponModifierSpear = 0f;
    [HideInInspector] public float weaponModifierHandgun = 0f;
    [HideInInspector] public float weaponModifierRifle = 0f;

    [HideInInspector] public float spellPower = 1f;

    [HideInInspector] public float spellFireDamageModifier = 0f;
    [HideInInspector] public float spellLightningDamageModifier = 0f;
    [HideInInspector] public float spellPoisonDamageModifier = 0f;

    //equipment bonuses
    //--------------------------------------------

    [HideInInspector] public int eqHealthBonus = 0;
    [HideInInspector] public int eqHealthRegenBonus = 0;
    [HideInInspector] public float eqSpeedBonus = 0f;

    [HideInInspector] public float eqPhysicalResistanceBonus = 0f;
    [HideInInspector] public float eqFireResistanceBonus = 0f;
    [HideInInspector] public float eqLightningResistanceBonus = 0f;
    [HideInInspector] public float eqPoisonResistanceBonus = 0f;

    [HideInInspector] public int eqDamageBonus = 0;
    [HideInInspector] public float eqAttackSpeedBonus = 0f;
    [HideInInspector] public int weaponDamage = 0;
    [HideInInspector] public float weaponAttackSpeed = 0f;

    [ReadOnly] public WeaponType weaponType = WeaponType.Unarmed;

    //effect bonuses
    //--------------------------------------------
    [HideInInspector] public int effectHealthBonus = 0;
    [HideInInspector] public int effectHealthRegenBonus = 0;
    [HideInInspector] public float effectSpeedBonus = 0f;

    [HideInInspector] public float effectPhysicalResistanceBonus = 0f;
    [HideInInspector] public float effectFireResistanceBonus = 0f;
    [HideInInspector] public float effectLightningResistanceBonus = 0f;
    [HideInInspector] public float effectPoisonResistanceBonus = 0f;

    [HideInInspector] public int effectDamageBonus = 0;
    [HideInInspector] public float effectAttackSpeedBonus = 0f;

    // Use this for initialization
    void Start()
    {
        effects = new List<Effect>();
        effectsToRemove = new List<Effect>();
        equippedItems = new List<Item>();

        health = maxHealth;
        damageType = DamageType.Physical;

        InvokeRepeating("UpdateEffects", 1f, 1f);
        InvokeRepeating("UpdateStats", 0f, 1f);
        InvokeRepeating("RegenerateHealth", 1f, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            OnDeath();
        }
        CheckConstraints();
        UpdateAgent();
    }

    private void OnDeath()
    {
        GameConsts.isPlayerDead = true;
        effects.Clear();
        CancelInvoke();
        Instantiate(deathEffect, transform.position, transform.rotation);
        gameObject.SetActive(false);
    }

    //---------------------
    //Stat increase effects
    //---------------------

    public void AddStrengthPoint()
    {
        strength++;
        // attribute bonuses
        //------------------------------
        weaponModifier1H += 0.02f;
        weaponModifierSpear += 0.03f;
        //------------------------------
    }

    public void AddDexterityPoint()
    {
        dexterity++;
        // attribute bonuses
        //------------------------------
        attackSpeedStatBonus += 0.01f;
        weaponModifierHandgun += 0.02f;
        weaponModifierRifle += 0.02f;
        //------------------------------
    }

    public void AddIntelligencePoint()
    {
        intelligence++;
        // attribute bonuses
        //------------------------------
        spellPower += 0.1f;
        spellFireDamageModifier += 0.02f;
        spellLightningDamageModifier += 0.02f;
        //------------------------------
    }

    public void AddEndurancePoint()
    {
        endurance++;
        // attribute bonuses
        //------------------------------
        maxHealthBase += 10;
        if (endurance % 10 == 0)
            healthRegenBase += 1;
        if (endurance % 2 == 0)
        {
            physicalResistanceBase += 0.01f;
        }
        if ((endurance - 10) % 3 == 0)
        {
            fireResistanceBase += 0.01f;
            lightningResistanceBase += 0.01f;
            poisonResistanceBase += 0.01f;
        }
        //------------------------------
    }

    public void GiveExperience(int experience)
    {
        experiencePoints += experience;
        if(experiencePoints >= experienceLevels[level])
        {
            LevelUp();
        }
    }

    private void LevelUp()
    {
        level++;
        if (LevelUpEvent != null)
        {
            LevelUpEvent();
        }
    }

    public void UpdateStats()
    {
        maxHealth = maxHealthBase + eqHealthBonus + effectHealthBonus;
        healthRegen = healthRegenBase + eqHealthRegenBonus + effectHealthRegenBonus;

        moveSpeed = moveSpeedBase * (1f + eqSpeedBonus + effectSpeedBonus);
        angularSpeed = angularSpeedBase * (1f + eqSpeedBonus + effectSpeedBonus);

        damage = weaponDamage;
        switch (weaponType)
        {
            case WeaponType.OneHanded:
                isAttackEnabled = true;
                damage += (int)(weaponDamage * weaponModifier1H);
                break;
            case WeaponType.Spear:
                isAttackEnabled = true;
                damage += (int)(weaponDamage * weaponModifierSpear);
                break;
            case WeaponType.Handgun:
                isAttackEnabled = true;
                damage += (int)(weaponDamage * weaponModifierHandgun);
                break;
            case WeaponType.Rifle:
                isAttackEnabled = true;
                damage += (int)(weaponDamage * weaponModifierRifle);
                break;
            default:
                isAttackEnabled = false;
                break;
        }

        attackSpeed = weaponAttackSpeed * (1f + attackSpeedStatBonus + eqAttackSpeedBonus + effectAttackSpeedBonus);

        physicalResistance = physicalResistanceBase + eqPhysicalResistanceBonus + effectPhysicalResistanceBonus;
        fireResistance = fireResistanceBase + eqFireResistanceBonus + effectFireResistanceBonus;
        lightningResistance = lightningResistanceBase + eqLightningResistanceBonus + effectLightningResistanceBonus;
        poisonResistance = poisonResistanceBase + eqPoisonResistanceBonus + effectPoisonResistanceBonus;
    }
}
