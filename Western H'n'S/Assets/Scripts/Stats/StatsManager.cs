﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections.Generic;

[RequireComponent(typeof(NavMeshAgent))]
public abstract class StatsManager : MonoBehaviour
{
    [Header("Base stats")]
    public int maxHealth = 100;
    public int health = 100;
    public int healthRegen = 1;
    public float moveSpeed = 10f;
    public float angularSpeed = 120f;

    [Header("Combat")]
    [Space]
    public DamageType damageType;
    public int damage = 10;
    public float attackSpeed = 1f;
    public float range = 5f;

    [Header("Resistances")]
    [Space]
    public float physicalResistance = 0f;
    public float fireResistance = 0f;
    public float lightningResistance = 0f;
    public float poisonResistance = 0f;

    [Header("Visuals")]
    [Space]
    public GameObject damageEffect;
    public GameObject deathEffect;

    [Header("Attack Effects")]
    [Space]
    public Effect[] attackEffects;

    protected List<Effect> effects;
    protected List<Effect> effectsToRemove;

    public void GiveDamage(DamageType type, int damage, Effect[] additionalEffects = null)
    {
        Instantiate(damageEffect, transform.position, transform.rotation);
        switch (type)
        {
            case DamageType.Physical:
                health -= (int)(damage * (1f - physicalResistance));
                break;
            case DamageType.Fire:
                health -= (int)(damage * (1f - fireResistance));
                break;
            case DamageType.Lightning:
                health -= (int)(damage * (1f - lightningResistance));
                break;
            case DamageType.Poison:
                health -= (int)(damage * (1f - poisonResistance));
                break;
        }
        if(additionalEffects != null)
        {
            foreach (Effect effect in additionalEffects)
            {
                AddEffect(effect);
            }
        }
        if (GetType() == typeof(EnemyStatsManager))
            ((EnemyStatsManager)this).isAttacked = true;
    }

    public void Heal(int health)
    {
        this.health += health;
    }

    public void UpdateAgent()
    {
        GetComponentInChildren<NavMeshAgent>().speed = moveSpeed;
        GetComponentInChildren<NavMeshAgent>().angularSpeed = angularSpeed;
    }

    public bool AddEffect(Effect effect)
    {
        if (Random.value <= effect.chanceToActivate)
        {
            Effect effectToAdd = effect.Initiate();
            if (effects.Find(x => x.name_.Equals(effectToAdd.name_)))
            {
                effectsToRemove.Add(effects.Find(x => x.name_.Equals(effectToAdd.name_)));
            }
            effects.Add(effectToAdd);
            effectToAdd.StartEffect(this);
            return true;
        }
        return false;
    }

    protected void UpdateEffects()
    {
        foreach (Effect effect in effectsToRemove)
        {
            effect.StopEffect(this);
            effects.Remove(effect);
        }
        effectsToRemove.Clear();
        foreach (Effect effect in effects)
        {
            if (effect.time <= 0)
            {
                effectsToRemove.Add(effect);
            }
            else
            {
                Instantiate(effect.visualEffect, transform.position, transform.rotation);
                effect.ActEverySecond(this);
            }
        }
    }

    protected void RegenerateHealth()
    {
        if (health < maxHealth)
            health += healthRegen;
    }

    protected void OnValidate() //just in case
    {
        UpdateAgent();
    }

    protected void CheckConstraints()
    {
        health = Mathf.Clamp(health, 0, maxHealth);
        physicalResistance = Mathf.Clamp(physicalResistance, GameConsts.MAX_VULNERABILITY, GameConsts.MAX_RESISTANCE);
        fireResistance = Mathf.Clamp(fireResistance, GameConsts.MAX_VULNERABILITY, GameConsts.MAX_RESISTANCE);
        lightningResistance = Mathf.Clamp(lightningResistance, GameConsts.MAX_VULNERABILITY, GameConsts.MAX_RESISTANCE);
        poisonResistance = Mathf.Clamp(poisonResistance, GameConsts.MAX_VULNERABILITY, GameConsts.MAX_RESISTANCE);
    }
}
