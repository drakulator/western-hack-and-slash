﻿using UnityEngine;
using System.Collections.Generic;

public class EnemyStatsManager : StatsManager
{
    [Header("Projectile")]
    [Space]
    public GameObject projectile;

    [Header("Detection")]
    [Space]
    public float detectionRange = 30f;
    [HideInInspector]
    public bool isAttacked = false;

    [Space]
    public int experienceOnDeath;

    private Highlight highlight;

    // Use this for initialization
    void Start()
    {
        effects = new List<Effect>();
        effectsToRemove = new List<Effect>();
        highlight = GetComponentInChildren<Highlight>();
        health = maxHealth;

        InvokeRepeating("RegenerateHealth", 1f, 1f);
        InvokeRepeating("UpdateEffects", 1f, 1f);
    }

    // Update is called once per frame
    void Update()
    {
        if (health <= 0)
        {
            OnDeath();
        }
        CheckConstraints();
        UpdateAgent();
    }

    private void OnDeath()
    {
        highlight.StopHighlighting();
        Instantiate(deathEffect, transform.position, transform.rotation);
        FindObjectOfType<PlayerStatsManager>().GiveExperience(experienceOnDeath);
        Destroy(gameObject);
    }
}
