﻿using UnityEngine;
using UnityEngine.UI;

public class ValueUpdate : MonoBehaviour
{
    public bool isLeftValue;
    public int value;

    private Text text;
    private Slider slider;

    // Use this for initialization
    void Start ()
    {
        text = GetComponentInChildren<Text>();
        slider = transform.parent.GetComponentInChildren<Slider>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (isLeftValue)
        {
            value = (int)slider.value;
        }
        else
        {
            value = (int)slider.maxValue - (int)slider.value + 1;
        }
        text.text = "" + value;
    }
}
