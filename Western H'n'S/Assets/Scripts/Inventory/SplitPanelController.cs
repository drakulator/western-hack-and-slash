﻿using UnityEngine;

public class SplitPanelController : MonoBehaviour
{
    public ValueUpdate leftValue;
    public ValueUpdate rightValue;
    public InventoryItem originalItem;
    public InventoryItem freeItem;

    public void SplitStack()
    {
        originalItem.SetStack(leftValue.value);
        freeItem.AddItem(originalItem.item, rightValue.value, originalItem.slotsManager.inventory);
        gameObject.SetActive(false);
        SlotsManager.isSplitPanelOpen = false;
    }

    public void Exit()
    {
        gameObject.SetActive(false);
        SlotsManager.isSplitPanelOpen = false;
    }
}
