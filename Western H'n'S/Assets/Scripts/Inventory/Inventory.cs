﻿using System.Collections.Generic;
using UnityEngine;

//Inventory master class
//There shouldn't be multiple instances of the same item in one inventory!!!
//Every object that utilizes slottable panels should derive from this class

public abstract class Inventory : MonoBehaviour
{
    public GameObject inventoryPanel;
    public GameObject slotsPanel;

    public List<ItemAmount> items = new List<ItemAmount>();

    protected SlotsManager slotsManager;

    public abstract void LoadInventory();
}
