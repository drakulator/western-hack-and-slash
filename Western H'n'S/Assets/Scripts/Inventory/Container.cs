﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class Container : Inventory
{
    private Highlight highlight;
    public float interactionDistance = 5f;
    public bool isActivated = false;

    private PlayerController player;
    private bool isTarget = false;

    // Use this for initialization
    void Start()
    {
        highlight = GetComponentInChildren<Highlight>();
        player = FindObjectOfType<PlayerController>();
        slotsManager = slotsPanel.GetComponent<SlotsManager>();
    }

    void Update()
    {
        if (isActivated && !inventoryPanel.activeSelf)
        {
            if (Vector3.Distance(player.transform.position, transform.position) < interactionDistance)
            {
                inventoryPanel.SetActive(true);
                inventoryPanel.GetComponentInChildren<SlotsManager>().inventory = this;
                LoadInventory();
                FindObjectOfType<PlayerController>().OpenInventoryPanel();
                isActivated = false;
                isTarget = false;
            }
        }
        if (isTarget && Input.GetMouseButton(0))
        {
            isActivated = false;
            isTarget = false;
        }
    }

    public IEnumerator WaitForInput()
    {
        yield return new WaitForSeconds(0.1f);
        isTarget = true;
    }

    public override void LoadInventory()
    {
        if (highlight != null)
        {
            inventoryPanel.transform.Find("Title Panel").GetComponentInChildren<Text>().text = highlight.name_;
        }
        slotsManager.LoadInventory(items);
    }
}
