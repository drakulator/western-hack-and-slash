﻿using UnityEngine;
using UnityEngine.UI;

public class PlayerInventory : Inventory
{
    public GameObject moneyDisplay;

    public int money = 0;

    // Use this for initialization
    void Start ()
    {
        slotsManager = slotsPanel.GetComponent<SlotsManager>();
        LoadInventory();
    }

    public override void LoadInventory()
    {
        slotsManager.LoadInventory(items);
        moneyDisplay.GetComponent<Text>().text = "" + money;
    }

    public void ChangeMoney(int money)
    {
        this.money += money;
        moneyDisplay.GetComponent<Text>().text = "" + this.money;
    }
}
