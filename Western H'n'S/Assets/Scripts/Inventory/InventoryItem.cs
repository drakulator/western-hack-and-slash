﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InventoryItem: MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    public Item item;
    public int amount;
    public bool isDragging = false;

    [HideInInspector]
    public SlotsManager slotsManager;

    protected Transform originalParent;

    void Start()
    {
        originalParent = transform.parent;
        slotsManager = originalParent.GetComponentInParent<SlotsManager>();
    }

    public virtual void AddItem(Item item, int amount, Inventory targetInventory = null)
    {
        if (targetInventory != null && targetInventory != slotsManager.inventory)
            slotsManager.inventory.items.AddOrIncrease(item, amount);
        this.item = item;
        SetStack(amount);
        GetComponent<Image>().sprite = item.sprite;
        GetComponent<Image>().color = new Color(1, 1, 1, 1);
    }

    public virtual void MoveItem(Inventory targetInventory)
    {
        if (targetInventory != null && targetInventory != slotsManager.inventory)
            slotsManager.inventory.items.RemoveOrDecrease(item, amount);
        item = null;
        SetStack(0);
        GetComponent<Image>().sprite = null;
        GetComponent<Image>().color = new Color(1, 1, 1, 0);
    }

    public virtual void RemoveItem()
    {
        slotsManager.inventory.items.RemoveOrDecrease(item, amount);
        item = null;
        SetStack(0);
        GetComponent<Image>().sprite = null;
        GetComponent<Image>().color = new Color(1, 1, 1, 0);
    }

    public virtual void UnslotItem()
    {
        item = null;
        SetStack(0);
        GetComponent<Image>().sprite = null;
        GetComponent<Image>().color = new Color(1, 1, 1, 0);
    }

    public void SetStack(int amount)
    {
        this.amount = amount;
        if (this.amount == 0 || this.amount == 1)
            GetComponentInChildren<Text>().text = "";
        else
            GetComponentInChildren<Text>().text = "" + this.amount;
    }

    public void ChangeStack(int amount)
    {
        if(-amount >= this.amount)
        {
            RemoveItem();
        }
        if(amount > 0)
        {
            this.amount += amount;
            slotsManager.inventory.items.AddOrIncrease(item, amount);
            if (this.amount < 0)
                this.amount = 0;
            if (this.amount == 0 || this.amount == 1)
                GetComponentInChildren<Text>().text = "";
            else
                GetComponentInChildren<Text>().text = "" + this.amount;
        }
        else if(amount < 0)
        {
            this.amount += amount;
            slotsManager.inventory.items.RemoveOrDecrease(item, -amount);
            if (this.amount < 0)
                this.amount = 0;
            if (this.amount == 0 || this.amount == 1)
                GetComponentInChildren<Text>().text = "";
            else
                GetComponentInChildren<Text>().text = "" + this.amount;
        }
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if(eventData.pointerId == -1 && item != null && !SlotsManager.isSplitPanelOpen)
        {
            slotsManager.HideTooltip();
            InventorySlot.originalSlot = GetComponentInParent<InventorySlot>();
            transform.SetParent(transform.root);
            transform.position = eventData.position;
            GetComponent<CanvasGroup>().blocksRaycasts = false;
            isDragging = true;
        }
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (item != null && isDragging)
        {
            transform.position = eventData.position;
        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        transform.SetParent(originalParent);
        transform.localPosition = Vector3.zero;
        GetComponent<CanvasGroup>().blocksRaycasts = true;
        isDragging = false;
    }
}
