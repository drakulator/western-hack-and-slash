﻿using UnityEngine;
using UnityEngine.EventSystems;

public class InventorySlot : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
    [HideInInspector]
    public InventoryItem inventoryItem;
    [HideInInspector]
    public SlotsManager slotsManager;

    public static InventorySlot originalSlot;

    // Use this for initialization
    void Start ()
    {
        inventoryItem = GetComponentInChildren<InventoryItem>();
        slotsManager = GetComponentInParent<SlotsManager>();
    }

    public virtual void OnDrop(PointerEventData eventData)
    {
        if (originalSlot != null && originalSlot.inventoryItem.item != null && originalSlot.inventoryItem.isDragging && !originalSlot.Equals(this))
        {
            if (inventoryItem.item != null)
            {
                if(inventoryItem.item.isStackable && inventoryItem.item.name_ == originalSlot.inventoryItem.item.name_)
                {
                    //change stack of items
                    inventoryItem.ChangeStack(originalSlot.inventoryItem.amount);
                    originalSlot.inventoryItem.RemoveItem();
                }
                else if(!(originalSlot is EquipmentSlot))
                {
                    //swap items
                    Item tempItem1 = inventoryItem.item;
                    Item tempItem2 = originalSlot.inventoryItem.item;
                    int tempAmount1 = inventoryItem.amount;
                    int tempAmount2 = originalSlot.inventoryItem.amount;
                    inventoryItem.MoveItem(originalSlot.slotsManager.inventory);
                    originalSlot.inventoryItem.MoveItem(slotsManager.inventory);
                    inventoryItem.AddItem(tempItem2, tempAmount2, originalSlot.slotsManager.inventory);
                    originalSlot.inventoryItem.AddItem(tempItem1, tempAmount1, slotsManager.inventory);
                }
            }
            else
            {
                //add item to empty slot
                inventoryItem.AddItem(originalSlot.inventoryItem.item, originalSlot.inventoryItem.amount, originalSlot.slotsManager.inventory);
                originalSlot.inventoryItem.MoveItem(slotsManager.inventory);
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if((originalSlot == null || (originalSlot != null && !originalSlot.inventoryItem.isDragging)) && inventoryItem.item != null)
        {
            slotsManager.ShowTooltip(inventoryItem.item, transform.position);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        slotsManager.HideTooltip();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.pointerId == -2 && inventoryItem.item != null)
        {
            slotsManager.ShowActionPanel(inventoryItem, transform.position);
        }
    }
}
