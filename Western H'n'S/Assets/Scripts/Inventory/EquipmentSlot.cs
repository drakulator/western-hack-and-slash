﻿using UnityEngine.EventSystems;

public class EquipmentSlot : InventorySlot
{
    public SlotType slotType;

    // Use this for initialization
    void Start()
    {
        inventoryItem = GetComponentInChildren<EquipmentItem>();
        slotsManager = GetComponentInParent<SlotsManager>();
    }

    public override void OnDrop(PointerEventData eventData)
    {
        if (originalSlot.inventoryItem.item != null && originalSlot.inventoryItem.isDragging && !originalSlot.Equals(this))
        {
            if(originalSlot.inventoryItem.item.slotType == slotType
                && ((EquipableItem)originalSlot.inventoryItem.item).AreRequirementsMet(FindObjectOfType<PlayerStatsManager>()))
            {
                if (inventoryItem.item != null)
                {
                    //swap items
                    Item tempItem1 = inventoryItem.item;
                    Item tempItem2 = originalSlot.inventoryItem.item;
                    int tempAmount1 = inventoryItem.amount;
                    int tempAmount2 = originalSlot.inventoryItem.amount;
                    inventoryItem.MoveItem(originalSlot.slotsManager.inventory);
                    originalSlot.inventoryItem.MoveItem(slotsManager.inventory);
                    inventoryItem.AddItem(tempItem2, tempAmount2, originalSlot.slotsManager.inventory);
                    originalSlot.inventoryItem.AddItem(tempItem1, tempAmount1, slotsManager.inventory);
                }
                else
                {
                    //add item to empty slot
                    inventoryItem.AddItem(originalSlot.inventoryItem.item, originalSlot.inventoryItem.amount, originalSlot.slotsManager.inventory);
                    originalSlot.inventoryItem.MoveItem(slotsManager.inventory);
                }
            }
        }
    }
}
