﻿using UnityEngine;
using UnityEngine.UI;

public class EquipmentItem : InventoryItem
{
    private PlayerStatsManager playerStats;

    void Start()
    {
        playerStats = FindObjectOfType<PlayerStatsManager>();
        originalParent = transform.parent;
        slotsManager = originalParent.GetComponentInParent<SlotsManager>();
    }

    public override void AddItem(Item item, int amount, Inventory targetInventory = null)
    {
        if (targetInventory != null && targetInventory != slotsManager.inventory)
            slotsManager.inventory.items.AddOrIncrease(item, amount);
        this.item = item;
        SetStack(amount);
        GetComponent<Image>().sprite = item.sprite;
        GetComponent<Image>().color = new Color(1, 1, 1, 1);
        playerStats.equippedItems.Add(item);
        ((EquipableItem)item).OnEquip(playerStats);
    }


    public override void MoveItem(Inventory targetInventory)
    {
        if (targetInventory != null && targetInventory != slotsManager.inventory)
            slotsManager.inventory.items.RemoveOrDecrease(item, amount);
        playerStats.equippedItems.Remove(item);
        ((EquipableItem)item).OnUnequip(playerStats);
        item = null;
        SetStack(0);
        GetComponent<Image>().sprite = null;
        GetComponent<Image>().color = new Color(1, 1, 1, 0);
    }

    public override void UnslotItem()
    {
        playerStats.equippedItems.Remove(item);
        ((EquipableItem)item).OnUnequip(playerStats);
        item = null;
        SetStack(0);
        GetComponent<Image>().sprite = null;
        GetComponent<Image>().color = new Color(1, 1, 1, 0);
    }

    public override void RemoveItem()
    {
        slotsManager.inventory.items.RemoveOrDecrease(item, amount);
        playerStats.equippedItems.Remove(item);
        ((EquipableItem)item).OnUnequip(playerStats);
        item = null;
        SetStack(0);
        GetComponent<Image>().sprite = null;
        GetComponent<Image>().color = new Color(1, 1, 1, 0);
    }
}
