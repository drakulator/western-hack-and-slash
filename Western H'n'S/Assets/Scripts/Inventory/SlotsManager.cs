﻿using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Collections;

public class SlotsManager : MonoBehaviour
{
    public Inventory inventory;
    public InventorySlot[] slots;

    public GameObject tooltipHolder;
    public GameObject splitPanelHolder;
    public GameObject actionPanel;

    public Coroutine waitForTooltip;

    [HideInInspector]
    public static bool isSplitPanelOpen;

    private InventoryItem selectedInventoryItem;

    private void OnValidate()
    {
        slots = GetComponentsInChildren<InventorySlot>();
    }

    public void EmptySlots()
    {
        foreach (InventorySlot slot in slots)
        {
            slot.inventoryItem.UnslotItem();
        }
    }

    public InventorySlot FindFreeSlot()
    {
        foreach (InventorySlot slot in slots)
        {
            if (slot.inventoryItem.item == null)
                return slot;
        }
        return null;
    }

    public void LoadInventory(List<ItemAmount> items)
    {
        foreach (ItemAmount itemAmount in items)
        {
            if (itemAmount != null)
            {
                foreach (InventorySlot itemSlot in slots)
                {
                    if (itemSlot.inventoryItem.item == null)
                    {
                        itemSlot.inventoryItem.AddItem(itemAmount.item, itemAmount.amount);
                        break;
                    }
                }
            }
        }
    }

    public void ShowActionPanel(InventoryItem inventoryItem, Vector3 position)
    {
        if(actionPanel != null)
        {
            selectedInventoryItem = inventoryItem;

            FitScreen(ref position);
            actionPanel.transform.position = position;

            CloseOpenPanels();
            actionPanel.gameObject.SetActive(true);
        }
    }

    public void ShowTooltip(Item item, Vector3 position)
    {
        tooltipHolder.transform.Find("Name").GetComponent<Text>().text = item.name_;
        tooltipHolder.transform.Find("Description").GetComponent<Text>().text = item.description;
        tooltipHolder.transform.Find("Cost").GetComponent<Text>().text = "Cost: " + item.cost;

        item.CompleteTooltip(tooltipHolder);

        FitScreen(ref position);
        tooltipHolder.transform.position = position;

        waitForTooltip = StartCoroutine(WaitBeforeShowingTooltip(1f));
    }

    public void HideTooltip()
    {
        if (waitForTooltip != null)
        {
            StopCoroutine(waitForTooltip);
        }
        tooltipHolder.SetActive(false);
    }

    private IEnumerator WaitBeforeShowingTooltip(float time)
    {
        yield return new WaitForSeconds(time);
        tooltipHolder.SetActive(true);
    }

    public void Use()
    {
        if(selectedInventoryItem.item is Consumable && ((Consumable)selectedInventoryItem.item).AreRequirementsMet(FindObjectOfType<PlayerStatsManager>()))
        {
            ((Consumable)selectedInventoryItem.item).OnUse();
            selectedInventoryItem.ChangeStack(-1);
            actionPanel.gameObject.SetActive(false);
        }
    }

    public void ShowSplitPanel()
    {
        InventorySlot freeSlot = FindFreeSlot();
        if (selectedInventoryItem.item != null && selectedInventoryItem.amount > 1 && freeSlot != null)
        {
            //splitPanelHolder.transform.position = transform.position;
            splitPanelHolder.GetComponentInChildren<Slider>().maxValue = selectedInventoryItem.amount - 1;
            splitPanelHolder.GetComponentInChildren<Slider>().value = 1; //splitPanelHolder.GetComponentInChildren<Slider>().minValue;
            splitPanelHolder.GetComponent<SplitPanelController>().freeItem = freeSlot.inventoryItem;
            splitPanelHolder.GetComponent<SplitPanelController>().originalItem = selectedInventoryItem;
            CloseOpenPanels();
            splitPanelHolder.gameObject.SetActive(true);
            actionPanel.gameObject.SetActive(false);
            isSplitPanelOpen = true;
        }
    }

    public void RemoveItem()
    {
        if (selectedInventoryItem.item != null)
        {
            selectedInventoryItem.RemoveItem();
        }
        actionPanel.gameObject.SetActive(false);
    }

    public void CloseActionPanel()
    {
        actionPanel.gameObject.SetActive(false);
    }

    public void FitScreen(ref Vector3 position)
    {
        float xOffset = 0.055f;
        float yOffset = 0.175f;
        if (position.x + Screen.width * xOffset > Screen.width)
        {
            position.x -= (Screen.width * xOffset);
        }
        else if (position.x < Screen.width * xOffset)
        {
            position.x += (Screen.width * xOffset);
        }
        if (position.y + Screen.height * yOffset > Screen.height)
        {
            position.y -= (Screen.height * yOffset);
        }
        else if (position.y < Screen.height * yOffset)
        {
            position.y += (Screen.height * yOffset);
        }
    }

    public static void CloseOpenPanels()
    {
        foreach (SlotsManager manager in FindObjectsOfType<SlotsManager>())
        {
            if(manager.tooltipHolder != null)
                manager.tooltipHolder.SetActive(false);
            if (manager.splitPanelHolder != null)
                manager.splitPanelHolder.SetActive(false);
            if (manager.actionPanel != null)
                manager.actionPanel.SetActive(false);
        }
    }
}
