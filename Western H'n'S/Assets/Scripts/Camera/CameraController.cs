﻿using UnityEngine;

public class CameraController : MonoBehaviour
{
    public bool isFollowing;

    public float xOffset;
    public float yOffset;
    public float zOffset;

    private PlayerController player;

    // Use this for initialization
    void Start()
    {
        player = FindObjectOfType<PlayerController>();
        isFollowing = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (isFollowing && !GameConsts.isPlayerDead)
            transform.position = new Vector3(player.transform.position.x + xOffset, player.transform.position.y + yOffset, player.transform.position.z + zOffset);
    }
}
