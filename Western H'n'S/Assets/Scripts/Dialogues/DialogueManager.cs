﻿using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour
{
    [HideInInspector]
    public const int MAX_RESPONSES = 4;

    //should be exactly one on each scene

    public GameObject textBox;
    public Text speakerText;
    public Text dialogueText;
    public float typeDelay = 0.01f;

    //private Coroutine lastRoutine = null;
    private PlayerController player;
    private PlayerResponse[] responses = new PlayerResponse[MAX_RESPONSES];
    private GameObject[] responseButtons = new GameObject[MAX_RESPONSES];
    private string speakerName;

    [HideInInspector]
    public ImportGameDataTxt dialogueImport;

    // Use this for initialization
    void Start ()
    {
        player = FindObjectOfType<PlayerController>();
        dialogueImport = new ImportGameDataTxt();
        for (int i = 0; i < MAX_RESPONSES; i++)
        {
            responseButtons[i] = textBox.transform.Find("Response " + (i + 1)).gameObject;
        }
    }

    public void DisableTextBox()
    {
        textBox.SetActive(false);
        player.isDialogueActive = false;
    }

    public void EnableTextBox()
    {
        textBox.SetActive(true);
        player.isDialogueActive = true;
        player.StopMovement();
    }

    public void OnDialogueChanged(DialogueNode dialogue, string speaker = "")
    {
        foreach (var response in responseButtons)
        {
            response.GetComponent<Button>().onClick.RemoveAllListeners();
            response.SetActive(false);
        }

        int length = 0;
        length = dialogue.responses.Length;
        if (speaker != "")
            speakerName = speaker;

        int n = 0;
        for (int i = 0; i < length; i++)
        {
            if (dialogue.responses[i].IsAvailable(this))
            {
                responses[i] = dialogue.responses[i];
                responseButtons[i].GetComponentInChildren<Text>().text = responses[i].text;
                responseButtons[i].GetComponent<Button>().onClick.AddListener(responses[i].Activate);
                responseButtons[i].SetActive(true);
                n++;
            }
        }
        if(n == 0)
        {
            Debug.Log("No responses available!!!");
        }
        speakerText.text = speakerName;
        dialogueText.text = dialogue.text;
    }

    public void ChooseResponse(PlayerResponse response)
    {
        response.OnChosen(this);
    }
}
