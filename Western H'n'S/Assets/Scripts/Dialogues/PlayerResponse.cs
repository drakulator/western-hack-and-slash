﻿using UnityEngine;

[System.Serializable]
public class PlayerResponse
{
    [TextArea(1, 3)]
    public string text;
    public DialogueNode nextNode;

    [Space]
    public Requirement[] gameDataValuesRequired;
    public Requirement[] gameDataValuesChanged;
    public DialogueResult[] dialogueResults;

    public bool IsAvailable(DialogueManager dialogueManager)
    {
        if(gameDataValuesRequired.Length > 0)
        {
            foreach (Requirement requirement in gameDataValuesRequired)
            {
                if (dialogueManager.dialogueImport.GetData(requirement.label) < requirement.value)
                    return false;
            }
        }
        return true;
    }

    public void Activate()
    {
        Object.FindObjectOfType<DialogueManager>().ChooseResponse(this);
    }

    public void OnChosen(DialogueManager dialogueManager)
    {
        if (gameDataValuesChanged.Length > 0)
        {
            foreach (Requirement requirement in gameDataValuesChanged)
            {
                dialogueManager.dialogueImport.SetData(requirement.label, requirement.value);
            }
            dialogueManager.dialogueImport.OnSave();
        }
        foreach (DialogueResult result in dialogueResults)
        {
            result.Execute();
        }
        if(nextNode != null)
        {
            dialogueManager.OnDialogueChanged(nextNode);
        }
        else
        {
            dialogueManager.DisableTextBox();
        }
    }
}
