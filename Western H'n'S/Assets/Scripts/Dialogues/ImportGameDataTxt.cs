﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ImportGameDataTxt
{
    private Dictionary<string, int> gameData;
    private TextAsset file;

    private string[] lines;

    public ImportGameDataTxt()
    {
        file = Resources.Load("Game Data") as TextAsset;
        try
        {
            gameData = new Dictionary<string, int>();

            string variable;
            int value;

            lines = file.text.Split('\n');

            foreach (string line in lines)
            {
                variable = line.Split('=')[0];
                value = Int32.Parse(line.Split('=')[1]);
                gameData.Add(variable, value);
            }
        }
        catch (Exception e)
        {
            Debug.Log(e.StackTrace);
            Debug.Log("Dialogue Data failed to load!");
        }
    }

    public int GetData(string variable)
    {
        int value;      
        return gameData.TryGetValue(variable, out value) ? value : -1;
    }

    public void ModifyData(string variable, int value)
    {
        if (gameData.ContainsKey(variable))
            gameData[variable] += value;
        else
            Debug.Log("Variable " + variable + " doesn't exist!");
    }

    public void SetData(string variable, int value)
    {
        if (gameData.ContainsKey(variable))
            gameData[variable] = value;
        else
        {
            gameData.Add(variable, value);
            Debug.Log("Variable " + variable + " added as new!");
        }
    }
    
    // ToDo
    public void OnSave()
    {

    }
}
