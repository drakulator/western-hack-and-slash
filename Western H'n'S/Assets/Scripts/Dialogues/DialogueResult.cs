﻿using UnityEngine;

public abstract class DialogueResult : ScriptableObject
{
    public abstract void Execute();
}
