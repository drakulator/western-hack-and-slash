﻿using UnityEngine;
using System;

[CreateAssetMenu(menuName = "Dialogues/DialogueNode")]
public class DialogueNode : ScriptableObject
{
    [TextArea(3, 10)]
    public string text;

    [Header("Maximum responses: see DialogueManager", order = 0)]
    [Space(-10, order = 1)]
    [Header("Should always have at least", order = 2)]
    [Space(-10, order = 3)]
    [Header("one available response!", order = 4)]
    [Space(10, order = 5)]

    public PlayerResponse[] responses = new PlayerResponse[1];

    private void OnEnable()
    {
        if(responses.Length > DialogueManager.MAX_RESPONSES)
            Array.Resize(ref responses, DialogueManager.MAX_RESPONSES);
    }
}