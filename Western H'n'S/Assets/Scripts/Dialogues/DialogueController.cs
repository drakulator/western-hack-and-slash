﻿using UnityEngine;
using System.Collections;

public class DialogueController : MonoBehaviour
{
    public DialogueNode startNode;
    public float interactionDistance = 10f;
    public bool isActivated = false;

    private DialogueManager dialogueManager;
    private PlayerController player;
    private string name_;
    private bool isTarget = false;

    // Use this for initialization
    void Start ()
    {
        dialogueManager = FindObjectOfType<DialogueManager>();
        name_ = GetComponentInChildren<Highlight>().name_;
        player = FindObjectOfType<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (isActivated)
        {
            if(Vector3.Distance(player.transform.position, transform.position) < interactionDistance)
            {
                InitiateDialogue();
                isActivated = false;
                isTarget = false;
            }
        }
        if(isTarget && Input.GetMouseButton(0))
        {
            isActivated = false;
            isTarget = false;
        }
    }

    public IEnumerator WaitForInput()
    {
        yield return new WaitForSeconds(0.1f);
        isTarget = true;
    }

    public void InitiateDialogue()
    {
        if (!dialogueManager.textBox.activeSelf)
        {
            dialogueManager.OnDialogueChanged(startNode, name_);
            dialogueManager.EnableTextBox();
        }
    }
}
