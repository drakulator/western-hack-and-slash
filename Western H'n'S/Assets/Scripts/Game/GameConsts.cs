﻿public enum DamageType
{
    Physical,
    Fire,
    Lightning,
    Poison
}

public enum WeaponType
{
    OneHanded,
    Spear,
    Handgun,
    Rifle,
    Unarmed
}

public enum SlotType
{
    Weapon,
    Hat,
    Armor,
    Gloves,
    Boots,
    Miscellaneous,
    Consumable,
    Other
}

public static class GameConsts
{
    [ReadOnly] public static bool isPlayerDead = false;

    public const float MAX_RESISTANCE = 0.8f;
    public const float MAX_VULNERABILITY = -1f;
    public const float LOSE_INTEREST_TIME = 10f;
}
