﻿using System.Collections.Generic;

public static class MyExtensions
{
    public static bool Contains(this List<ItemAmount> list, Item item)
    {
        foreach (ItemAmount itemAmount in list)
        {
            if (itemAmount.item == item)
                return true;
        }
        return false;
    }

    public static void AddOrIncrease(this List<ItemAmount> list, Item item, int amount)
    {
        if (item.isStackable && list.Contains(item))
        {
            list.Find(x => x.item == item).amount += amount;
        }
        else
        {
            list.Add(new ItemAmount(item, amount));
        }
    }

    public static void RemoveOrDecrease(this List<ItemAmount> list, Item item, int amount)
    {
        if (list.Contains(item))
        {
            ItemAmount itemToRemove = list.Find(x => x.item == item);
            if (itemToRemove.amount > amount)
            {
                itemToRemove.amount -= amount;
            }
            else
            {
                list.Remove(itemToRemove);
            }
        }
    }

    public static void Remove(this List<ItemAmount> list, Item item)
    {
        if (list.Contains(item))
        {
             list.Remove(list.Find(x => x.item == item));
        }
    }
}