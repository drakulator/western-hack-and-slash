﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Highlight : MonoBehaviour
{
    public string name_;
    public string title;

    [Space]
    public float rHighlight = 0.2f;
    public float gHighlight = 0.15f;
    public float bHighlight = 0.05f;

    private List<Color> startColors;
    private GameObject floatingText;
    private Text nameComponent;
    private Text titleComponent;
    private GameObject healthComponent;

    private EnemyStatsManager statManager;
    private DialogueController dialogue;
    private Container container;

    // Use this for initialization
    void Start()
    {
        floatingText = GameObject.Find("Canvas").GetComponent<FloatingTextHolder>().floatingText;
        nameComponent = GameObject.Find("Canvas").GetComponent<FloatingTextHolder>().nameComponent;
        titleComponent = GameObject.Find("Canvas").GetComponent<FloatingTextHolder>().titleComponent;
        healthComponent = GameObject.Find("Canvas").GetComponent<FloatingTextHolder>().healthComponent;

        statManager = GetComponentInParent<EnemyStatsManager>();
        dialogue = GetComponentInParent<DialogueController>();
        container = GetComponentInParent<Container>();

        startColors = new List<Color>();
        foreach (var model in GetComponentsInChildren<Renderer>())
        {
            foreach (var material in model.materials)
            {
                startColors.Add(material.color);
            }
        }
    }

    private void OnMouseEnter()
    {
        UpdateName();
        StartHighlighting();

        if (statManager == null)
            healthComponent.SetActive(false);
        else
            healthComponent.GetComponent<Slider>().maxValue = statManager.maxHealth;
    }

    private void OnMouseOver()
    {
        if(statManager != null)
            UpdateHealth();
    }

    private void OnMouseDown()
    {
        if (dialogue != null)
        {
            dialogue.isActivated = true;
            dialogue.StartCoroutine(dialogue.WaitForInput());
        }
        if (container != null)
        {
            container.isActivated = true;
            container.StartCoroutine(container.WaitForInput());
        }
    }

    private void OnMouseExit()
    {
        StopHighlighting();

        if (statManager == null)
            healthComponent.SetActive(true);
    }

    private void UpdateName()
    {
        nameComponent.text = name_;
        titleComponent.text = title;
        switch (transform.root.tag)
        {
            case "Non-Hostile":
                nameComponent.color = Color.yellow;
                titleComponent.color = Color.yellow;
                break;

            case "Ally":
                nameComponent.color = Color.green;
                titleComponent.color = Color.green;
                break;

            case "Enemy":
                nameComponent.color = Color.red;
                titleComponent.color = Color.red;
                break;

            case "Boss":
                nameComponent.color = new Color(1f, 0f, 1f);
                titleComponent.color = new Color(1f, 0f, 1f);
                break;

            default:
                nameComponent.color = Color.white;
                titleComponent.color = Color.white;
                break;
        }
    }

    private void UpdateHealth()
    {
        healthComponent.GetComponent<Slider>().value = statManager.health;
    }

    public void StartHighlighting()
    {
        foreach (var model in GetComponentsInChildren<Renderer>())
        {
            foreach (var material in model.materials)
            {
                material.color = new Color(material.color.r + rHighlight, material.color.g + gHighlight, material.color.b + bHighlight);
            }
        }
        floatingText.SetActive(true);
    }

    public void StopHighlighting()
    {
        int i = 0;
        foreach (var model in GetComponentsInChildren<Renderer>())
        {
            foreach (var material in model.materials)
            {
                material.color = startColors[i];
                i++;
            }
        }
        floatingText.SetActive(false);
    }
}
