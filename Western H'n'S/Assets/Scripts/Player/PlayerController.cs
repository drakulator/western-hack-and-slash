﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class PlayerController : MonoBehaviour
{
    [HideInInspector]
    public bool isDialogueActive;
    public GameObject inventoryPanel;
    public GameObject characterSheet;
    public GameObject containerPanel;

    private NavMeshAgent agent;
    private PlayerStatsManager playerStats;
    private float distance;
    private Vector3 target;
    private bool hasTarget = false;
    private EnemyStatsManager targetStats = null;
    private bool isWaiting = false;
    private bool isPanelOpen = false;

    private GameObject tooltipPanel;
    private GameObject splitPanel;
    private GameObject actionPanel;

    // Use this for initialization
    void Start () {
        playerStats = GetComponent<PlayerStatsManager>();
        agent = GetComponent<NavMeshAgent>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Inventory") && !isPanelOpen)
        {
            OpenInventoryPanel();
        }
        else if (Input.GetButtonDown("Character Sheet") && !isPanelOpen)
        {
            OpenCharacterSheet();
        }
        else if (Input.GetButtonDown("Inventory") && isPanelOpen || Input.GetButtonDown("Character Sheet") && isPanelOpen)
        {
            CloseCharacterSheet();
            CloseInventory();
            isPanelOpen = false;
        }

        if (Input.GetMouseButton(0) && !isWaiting && !isDialogueActive && !isPanelOpen)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 100))
            {
                EnableMovement();
                if (hit.collider.transform.root.tag == "Enemy" || hit.collider.transform.root.tag == "Boss")
                {
                    target = hit.collider.transform.position;
                    targetStats = hit.collider.GetComponentInParent<EnemyStatsManager>();
                    hasTarget = true;
                }
                else if (playerStats.projectile != null && Input.GetButton("Fire in Place"))
                {
                    target = new Vector3(hit.point.x, transform.position.y, hit.point.z);
                    hasTarget = true;
                }
                else
                {
                    agent.destination = hit.point;
                    hasTarget = false;
                }

                if (hasTarget)
                {
                    distance = Vector3.Distance(transform.position, hit.point);
                    if (distance <= playerStats.range)
                    {
                        StopMovement();

                        // Rotate towards target
                        Vector3 direction = (transform.position - hit.collider.transform.position).normalized;
                        Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
                        transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, Time.deltaTime * playerStats.angularSpeed);

                        if (playerStats.isAttackEnabled)
                            StartCoroutine(Attack(playerStats.attackSpeed));
                    }
                    else
                    {
                        agent.destination = hit.point;
                    }
                }
            }
            if (Vector3.Distance(agent.transform.position, agent.destination) < 0.1f)
                StopMovement();
        }
	}

    private IEnumerator Attack(float attackSpeed)
    {
        isWaiting = true;
        //animator.SetTrigger("Attack");
        yield return new WaitForSeconds(0.5f / attackSpeed);
        if (playerStats.projectile == null)
        {
            targetStats.GiveDamage(playerStats.damageType, playerStats.damage, playerStats.attackEffects);
        }
        else
        {
            Shoot();
        }
        yield return new WaitForSeconds(0.5f / attackSpeed);
        isWaiting = false;
    }

    public void StopMovement()
    {
        agent.isStopped = true;
    }

    public void EnableMovement()
    {
        agent.isStopped = false;
    }

    public void OpenInventoryPanel()
    {
        isPanelOpen = true;
        inventoryPanel.SetActive(true);
        StopMovement();
    }

    public void OpenCharacterSheet()
    {
        isPanelOpen = true;
        characterSheet.SetActive(true);
        StopMovement();
    }

    public void CloseCharacterSheet()
    {
        if (characterSheet.activeSelf)
        {
            characterSheet.SetActive(false);
        }
        isPanelOpen = false;
    }

    public void CloseInventory()
    {
        if (inventoryPanel.activeSelf)
        {
            DeactivateInventoryChildPanels(inventoryPanel);
            inventoryPanel.SetActive(false);
        }
        if (containerPanel.activeSelf)
        {
            DeactivateInventoryChildPanels(containerPanel);
            containerPanel.GetComponentInChildren<SlotsManager>().EmptySlots();
            containerPanel.SetActive(false);
        }
        isPanelOpen = false;
    }

    public void DeactivateInventoryChildPanels(GameObject panel)
    {
        tooltipPanel = panel.transform.Find("Tooltip Panel").gameObject;
        tooltipPanel.SetActive(false);
        splitPanel = panel.transform.Find("Split Panel").gameObject;
        splitPanel.SetActive(false);
        actionPanel = panel.transform.Find("Action Panel").gameObject;
        actionPanel.SetActive(false);
        SlotsManager.isSplitPanelOpen = false;
    }

    public void Shoot()
    {
        var projectile = Instantiate(playerStats.projectile, transform.position, transform.rotation);
        projectile.GetComponent<Projectile>().Fire(target, playerStats.damageType, playerStats.damage, playerStats.range, 0, playerStats.attackEffects);
    }
}
