﻿using UnityEngine;

public class IdleAnimationController : MonoBehaviour
{
    public float minChangeAnimTime = 7f;
    public float maxChangeAnimTime = 12f;

    public int animations = 2;

    private float timeToChangeAnim;

    private Animator animator;

	// Use this for initialization
	void Start () {
        timeToChangeAnim = Random.Range(minChangeAnimTime, maxChangeAnimTime);
        animator = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        timeToChangeAnim -= Time.deltaTime;

        if(timeToChangeAnim <= 0f)
        {
            animator.SetInteger("Play Idle", Random.Range(1, animations + 1));
            timeToChangeAnim = Random.Range(minChangeAnimTime, maxChangeAnimTime);
        }
    }
}
