﻿using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Items/Weapon")]
public class Weapon : EquipableItem
{
    public int damage = 0;
    public float attackSpeed = 0f;
    public float range = 0f;

    [Space]
    public GameObject projectile;
    public WeaponType type;
    public Effect[] effects;

    public int healthBonus = 0;
    public int healthRegenBonus = 0;
    [Tooltip("Percentage")]
    public float speedBonus = 0f;

    public float fireResistanceBonus = 0f;
    public float lightningResistanceBonus = 0f;
    public float poisonResistanceBonus = 0f;

    public Weapon()
    {
        isStackable = false;
        slotType = SlotType.Weapon;
    }

    public override void OnEquip(PlayerStatsManager playerStats)
    {
        playerStats.range = range;
        playerStats.projectile = projectile;
        playerStats.weaponType = type;
        playerStats.weaponDamage = damage;
        playerStats.weaponAttackSpeed = attackSpeed;
        playerStats.attackEffects = effects;

        playerStats.eqHealthBonus += healthBonus;
        playerStats.eqHealthRegenBonus += healthRegenBonus;
        playerStats.eqSpeedBonus += speedBonus;

        playerStats.eqFireResistanceBonus += fireResistanceBonus;
        playerStats.eqLightningResistanceBonus += lightningResistanceBonus;
        playerStats.eqPoisonResistanceBonus += poisonResistanceBonus;

        playerStats.UpdateStats();
    }

    public override void OnUnequip(PlayerStatsManager playerStats)
    {
        playerStats.range = 7f;
        playerStats.projectile = null;
        playerStats.weaponType = WeaponType.Unarmed;
        playerStats.weaponDamage = 0;
        playerStats.weaponAttackSpeed = 0f;
        playerStats.attackEffects = null;

        playerStats.eqHealthBonus -= healthBonus;
        playerStats.eqHealthRegenBonus -= healthRegenBonus;
        playerStats.eqSpeedBonus -= speedBonus;

        playerStats.eqFireResistanceBonus -= fireResistanceBonus;
        playerStats.eqLightningResistanceBonus -= lightningResistanceBonus;
        playerStats.eqPoisonResistanceBonus -= poisonResistanceBonus;

        playerStats.UpdateStats();
    }

    public override void CompleteTooltip(GameObject tooltipHolder)
    {
        string stringToUpdate = "Requirements: \n";

        if(strengthRequirement > 0)
            stringToUpdate += strengthRequirement + " Strength\n";
        if (dexterityRequirement > 0)
            stringToUpdate += dexterityRequirement + " Dexterity\n";
        if (intelligenceRequirement > 0)
            stringToUpdate += intelligenceRequirement + " Intelligence\n";
        if (enduranceRequirement > 0)
            stringToUpdate += enduranceRequirement + " Endurance\n";

        stringToUpdate = stringToUpdate.TrimEnd('\n');
        if (stringToUpdate == "Requirements: ")
        {
            tooltipHolder.transform.Find("Requirements").gameObject.SetActive(false);
        }
        else
        {
            tooltipHolder.transform.Find("Requirements").gameObject.SetActive(true);
            tooltipHolder.transform.Find("Requirements").GetComponent<Text>().text = stringToUpdate;
        }

        tooltipHolder.transform.Find("Stats").gameObject.SetActive(true);
        tooltipHolder.transform.Find("Stats").GetComponent<Text>().text ="Weapon Type: " + type + "\nDamage: " + damage + "\n" + 
            "Attack Speed: " + attackSpeed;

        stringToUpdate = "";

        if (healthBonus > 0)
            stringToUpdate += "Health +" + healthBonus + "\n";
        if (healthRegenBonus > 0)
            stringToUpdate += "Health Regeneration +" + healthRegenBonus + "\n";
        if (speedBonus > 0)
            stringToUpdate += "Move Speed +" + speedBonus * 100f + "%\n";
        if (fireResistanceBonus > 0)
            stringToUpdate += "Fire Resistance +" + fireResistanceBonus * 100f + "%\n";
        if (lightningResistanceBonus > 0)
            stringToUpdate += "Lightning Resistance +" + lightningResistanceBonus * 100f + "%\n";
        if (poisonResistanceBonus > 0)
            stringToUpdate += "Poison Resistance +" + poisonResistanceBonus * 100f + "%\n";

        stringToUpdate = stringToUpdate.TrimEnd('\n');
        if (stringToUpdate == "")
        {
            tooltipHolder.transform.Find("Bonuses").gameObject.SetActive(false);
        }
        else
        {
            tooltipHolder.transform.Find("Bonuses").gameObject.SetActive(true);
            tooltipHolder.transform.Find("Bonuses").GetComponent<Text>().text = stringToUpdate;
        }

        stringToUpdate = "";

        if (healthBonus < 0)
            stringToUpdate += "Health " + healthBonus + "\n";
        if (healthRegenBonus < 0)
            stringToUpdate += "Health Regeneration " + healthRegenBonus + "\n";
        if (speedBonus < 0)
            stringToUpdate += "Move Speed " + speedBonus * 100f + "%\n";
        if (fireResistanceBonus < 0)
            stringToUpdate += "Fire Resistance " + fireResistanceBonus * 100f + "%\n";
        if (lightningResistanceBonus < 0)
            stringToUpdate += "Lightning Resistance " + lightningResistanceBonus * 100f + "%\n";
        if (poisonResistanceBonus < 0)
            stringToUpdate += "Poison Resistance " + poisonResistanceBonus * 100f + "%\n";

        stringToUpdate = stringToUpdate.TrimEnd('\n');
        if (stringToUpdate == "")
        {
            tooltipHolder.transform.Find("Penalties").gameObject.SetActive(false);
        }
        else
        {
            tooltipHolder.transform.Find("Penalties").gameObject.SetActive(true);
            tooltipHolder.transform.Find("Penalties").GetComponent<Text>().text = stringToUpdate;
        }

        stringToUpdate = "On hit: \n";

        if (effects != null)
        {
            foreach (Effect effect in effects)
            {
                stringToUpdate += effect.ToString() + "\n";
            }
        }

        stringToUpdate = stringToUpdate.TrimEnd('\n');
        if (stringToUpdate == "On hit: ")
        {
            tooltipHolder.transform.Find("Additional Effects").gameObject.SetActive(false);
        }
        else
        {
            tooltipHolder.transform.Find("Additional Effects").gameObject.SetActive(true);
            tooltipHolder.transform.Find("Additional Effects").GetComponent<Text>().text = stringToUpdate;
        }
    }
}
