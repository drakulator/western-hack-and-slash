﻿[System.Serializable]
public class ItemAmount
{
    public Item item;
    public int amount;

    public ItemAmount(Item item, int amount = 1)
    {
        this.item = item;
        this.amount = amount;
    }

    public ItemAmount()
    {
        item = null;
        amount = 0;
    }
}
