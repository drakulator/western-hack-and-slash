﻿using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(menuName = "Items/Consumable")]
public class Consumable : Item
{
    public int strengthRequirement;
    public int dexterityRequirement;
    public int intelligenceRequirement;
    public int enduranceRequirement;

    public Effect[] positiveEffects;
    public Effect[] negativeEffects;

    public Consumable()
    {
        isStackable = true;
        slotType = SlotType.Consumable;
    }

    public void OnUse()
    {
        if(positiveEffects != null)
        {
            foreach (Effect effect in positiveEffects)
            {
                FindObjectOfType<PlayerStatsManager>().AddEffect(effect);
            }
        }
        if (negativeEffects != null)
        {
            foreach (Effect effect in negativeEffects)
            {
                FindObjectOfType<PlayerStatsManager>().AddEffect(effect);
            }
        }
    }

    public bool AreRequirementsMet(PlayerStatsManager playerStats)
    {
        if (playerStats.strength < strengthRequirement)
            return false;
        if (playerStats.dexterity < dexterityRequirement)
            return false;
        if (playerStats.intelligence < intelligenceRequirement)
            return false;
        if (playerStats.endurance < enduranceRequirement)
            return false;
        return true;
    }

    public override void CompleteTooltip(GameObject tooltipHolder)
    {
        string stringToUpdate = "Requirements: \n";

        if (strengthRequirement > 0)
            stringToUpdate += strengthRequirement + " Strength\n";
        if (dexterityRequirement > 0)
            stringToUpdate += dexterityRequirement + " Dexterity\n";
        if (intelligenceRequirement > 0)
            stringToUpdate += intelligenceRequirement + " Intelligence\n";
        if (enduranceRequirement > 0)
            stringToUpdate += enduranceRequirement + " Endurance\n";

        stringToUpdate = stringToUpdate.TrimEnd('\n');
        if (stringToUpdate == "Requirements: ")
        {
            tooltipHolder.transform.Find("Requirements").gameObject.SetActive(false);
        }
        else
        {
            tooltipHolder.transform.Find("Requirements").gameObject.SetActive(true);
            tooltipHolder.transform.Find("Requirements").GetComponent<Text>().text = stringToUpdate;
        }

        tooltipHolder.transform.Find("Stats").gameObject.SetActive(false);

        stringToUpdate = "";

        stringToUpdate = "On use: \n";

        if (positiveEffects != null)
        {
            foreach (Effect effect in positiveEffects)
            {
                stringToUpdate += effect.ToString() + "\n";
            }
        }

        stringToUpdate = stringToUpdate.TrimEnd('\n');
        if (stringToUpdate == "On use: ")
        {
            tooltipHolder.transform.Find("Bonuses").gameObject.SetActive(false);
        }
        else
        {
            tooltipHolder.transform.Find("Bonuses").gameObject.SetActive(true);
            tooltipHolder.transform.Find("Bonuses").GetComponent<Text>().text = stringToUpdate;
        }

        stringToUpdate = "";

        stringToUpdate = "On use: \n";

        if (negativeEffects != null)
        {
            foreach (Effect effect in negativeEffects)
            {
                stringToUpdate += effect.ToString() + "\n";
            }
        }

        stringToUpdate = stringToUpdate.TrimEnd('\n');
        if (stringToUpdate == "On use: ")
        {
            tooltipHolder.transform.Find("Penalties").gameObject.SetActive(false);
        }
        else
        {
            tooltipHolder.transform.Find("Penalties").gameObject.SetActive(true);
            tooltipHolder.transform.Find("Penalties").GetComponent<Text>().text = stringToUpdate;
        }

        tooltipHolder.transform.Find("Additional Effects").gameObject.SetActive(false);
    }
}
