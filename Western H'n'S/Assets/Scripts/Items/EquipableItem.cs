﻿public abstract class EquipableItem : Item
{
    public int strengthRequirement;
    public int dexterityRequirement;
    public int intelligenceRequirement;
    public int enduranceRequirement;

    public abstract void OnEquip(PlayerStatsManager playerStats);
    public abstract void OnUnequip(PlayerStatsManager playerStats);

    public bool AreRequirementsMet(PlayerStatsManager playerStats)
    {
        if (playerStats.strength < strengthRequirement)
            return false;
        if (playerStats.dexterity < dexterityRequirement)
            return false;
        if (playerStats.intelligence < intelligenceRequirement)
            return false;
        if (playerStats.endurance < enduranceRequirement)
            return false;
        return true;
    }
}
