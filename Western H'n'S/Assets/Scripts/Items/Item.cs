﻿using UnityEngine;

public abstract class Item : ScriptableObject
{
    [Tooltip("Unique, objects having the same name can be stacked!")]
    public string name_;
    public int cost;
    [ReadOnly] public bool isStackable;
    [TextArea(3, 10)]
    public string description;
    public Sprite sprite;
    public SlotType slotType;

    public abstract void CompleteTooltip(GameObject tooltipHolder);
}
