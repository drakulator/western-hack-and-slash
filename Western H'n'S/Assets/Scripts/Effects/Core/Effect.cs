﻿using UnityEngine;

public abstract class Effect : ScriptableObject
{
    public string name_;
    public int time;
    public float chanceToActivate;
    public GameObject visualEffect;

    public abstract void StartEffect(StatsManager controller);
    public abstract void ActEverySecond(StatsManager controller);
    public abstract void StopEffect(StatsManager controller);
    public abstract Effect Initiate();
}
