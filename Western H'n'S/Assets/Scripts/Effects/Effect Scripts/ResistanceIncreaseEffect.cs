﻿using UnityEngine;

[CreateAssetMenu(menuName = "Effects/ResistanceIncreaseEffect")]
public class ResistanceIncreaseEffect : Effect
{
    public float fireResistanceBonus;
    public float lightningResistanceBonus;
    public float poisonResistanceBonus;
    public float physicalResistanceBonus;

    public override void StartEffect(StatsManager controller)
    {
        if(controller is PlayerStatsManager)
        {
            ((PlayerStatsManager)controller).effectFireResistanceBonus += fireResistanceBonus;
            ((PlayerStatsManager)controller).effectLightningResistanceBonus += lightningResistanceBonus;
            ((PlayerStatsManager)controller).effectPoisonResistanceBonus += poisonResistanceBonus;
            ((PlayerStatsManager)controller).effectPhysicalResistanceBonus += physicalResistanceBonus;
        }
        else
        {
            controller.fireResistance += fireResistanceBonus;
            controller.fireResistance += lightningResistanceBonus;
            controller.fireResistance += poisonResistanceBonus;
            controller.fireResistance += physicalResistanceBonus;
        }
        Debug.Log("ResistanceIncreaseEffect " + name_ + " activated");
    }

    public override void ActEverySecond(StatsManager controller)
    {
        time--;
        Debug.Log("ResistanceIncreaseEffect " + name_ + " lasts");
    }

    public override void StopEffect(StatsManager controller)
    {
        if (controller is PlayerStatsManager)
        {
            ((PlayerStatsManager)controller).effectFireResistanceBonus -= fireResistanceBonus;
            ((PlayerStatsManager)controller).effectLightningResistanceBonus -= lightningResistanceBonus;
            ((PlayerStatsManager)controller).effectPoisonResistanceBonus -= poisonResistanceBonus;
            ((PlayerStatsManager)controller).effectPhysicalResistanceBonus -= physicalResistanceBonus;
        }
        else
        {
            controller.fireResistance -= fireResistanceBonus;
            controller.fireResistance -= lightningResistanceBonus;
            controller.fireResistance -= poisonResistanceBonus;
            controller.fireResistance -= physicalResistanceBonus;
        }
        Debug.Log("ResistanceIncreaseEffect " + name_ + " stopped");
    }

    public override string ToString()
    {
        string resistanceList = "";

        if(fireResistanceBonus > 0f)
        {
            resistanceList += fireResistanceBonus * 100 + "% fire, ";
        }
        if (lightningResistanceBonus > 0f)
        {
            resistanceList += lightningResistanceBonus * 100 + "% lightning, ";
        }
        if (poisonResistanceBonus > 0f)
        {
            resistanceList += poisonResistanceBonus * 100 + "% poison, ";
        }
        if (physicalResistanceBonus > 0f)
        {
            resistanceList += physicalResistanceBonus * 100 + "% physical, ";
        }
        resistanceList = resistanceList.Remove(resistanceList.Length - 2);
        return chanceToActivate * 100 + "% for " + resistanceList + " resistance for " + time + " seconds\n";
    }

    public override Effect Initiate()
    {
        ResistanceIncreaseEffect effect = CreateInstance<ResistanceIncreaseEffect>();

        effect.name_ = name_;
        effect.time = time;
        effect.chanceToActivate = chanceToActivate;
        effect.visualEffect = visualEffect;
        effect.fireResistanceBonus = fireResistanceBonus;
        effect.lightningResistanceBonus = lightningResistanceBonus;
        effect.poisonResistanceBonus = poisonResistanceBonus;
        effect.physicalResistanceBonus = physicalResistanceBonus;

        return effect;
    }
}
