﻿using UnityEngine;

[CreateAssetMenu(menuName = "Effects/HealEffect")]
public class HealEffect : Effect
{
    public int healing;

    public override void StartEffect(StatsManager controller)
    {
        Debug.Log("HealEffect " + name_ + " activated");
    }

    public override void ActEverySecond(StatsManager controller)
    {
        controller.Heal(healing);
        time--;
        Debug.Log("HealEffect " + name_ + " lasts");
    }

    public override void StopEffect(StatsManager controller)
    {
        Debug.Log("HealEffect " + name_ + " stopped");
    }

    public override string ToString()
    {
        return chanceToActivate * 100 + "% for " + healing + " points of Healing for " + time + " seconds\n";
    }

    public override Effect Initiate()
    {
        HealEffect effect = CreateInstance<HealEffect>();

        effect.name_ = name_;
        effect.time = time;
        effect.chanceToActivate = chanceToActivate;
        effect.visualEffect = visualEffect;
        effect.healing = healing;

        return effect;
    }
}
