﻿using UnityEngine;

[CreateAssetMenu(menuName = "Effects/DamageOverTimeEffect")]
public class DamageOverTimeEffect : Effect
{
    public DamageType damageType;
    public int damage;

    public override void StartEffect(StatsManager controller)
    {
        Debug.Log("DamageOverTimeEffect " + name_ + " activated");
    }

    public override void ActEverySecond(StatsManager controller)
    {
        controller.GiveDamage(damageType, damage);
        time--;
        Debug.Log("DamageOverTimeEffect " + name_ + " lasts");
    }

    public override void StopEffect(StatsManager controller)
    {
        Debug.Log("DamageOverTimeEffect " + name_ + " stopped");
    }

    public override string ToString()
    {
        return chanceToActivate * 100 + "% for " + damage + " points " + damageType + " Damage for " + time + " seconds\n";
    }

    public override Effect Initiate()
    {
        DamageOverTimeEffect effect = CreateInstance<DamageOverTimeEffect>();

        effect.name_ = name_;
        effect.time = time;
        effect.chanceToActivate = chanceToActivate;
        effect.visualEffect = visualEffect;
        effect.damageType = damageType;
        effect.damage = damage;

        return effect;
    }
}
