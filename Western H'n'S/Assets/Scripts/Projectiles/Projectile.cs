﻿using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float speed;
    [HideInInspector]
    public Vector3 target;
    [HideInInspector]
    public DamageType damageType;
    [HideInInspector]
    public int damage;
    [HideInInspector]
    public float range;
    [HideInInspector]
    public int source; //0 - player, 1 - enemy, 2 - trap
    [HideInInspector]
    public Effect[] effects;

    private Vector3 startingPoint;

    // Use this for initialization
    void Start()
    {
        startingPoint = transform.position;
        GetComponent<Rigidbody>().velocity = (target - transform.position).normalized * speed;
        Destroy(gameObject, 10f);
    }

    // Update is called once per frame
    void Update()
    {
        if(Vector3.Distance(startingPoint, transform.position) > range)
        {
            Destroy(gameObject);
        }
    }
        private void OnTriggerEnter(Collider other)
    {
        if (((other.transform.root.tag == "Enemy" || other.transform.root.tag == "Boss") && (source == 0 || source == 2)) 
            || ((other.transform.root.tag == "Player") && (source == 1 || source == 2)))
        {
            other.GetComponentInParent<StatsManager>().GiveDamage(damageType, damage, effects);
            Destroy(gameObject);
        }
        else if(other.transform.root.tag != "Enemy" && other.transform.root.tag != "Boss" && other.transform.root.tag != "Player")
        {
            Destroy(gameObject);
        }
    }

    public void Fire(Vector3 target, DamageType damageType, int damage, float range, int source /*0 - player, 1 - enemy, 2 - trap*/, Effect[] effects = null)
    {
        this.target = target;
        this.damageType = damageType;
        this.damage = damage;
        this.range = range;
        this.source = source;
        this.effects = effects;
    }
}
