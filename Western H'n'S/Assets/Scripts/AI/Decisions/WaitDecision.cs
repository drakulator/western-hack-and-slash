﻿using UnityEngine;

[CreateAssetMenu(menuName = "AI/Decisions/Wait")]
public class WaitDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        if (Vector3.Distance(controller.transform.position, controller.startPosition) < controller.agent.stoppingDistance + 0.1f)
            return true;
        else
            return false;
    }
}
