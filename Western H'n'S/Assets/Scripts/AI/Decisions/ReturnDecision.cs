﻿using UnityEngine;

[CreateAssetMenu(menuName = "AI/Decisions/Return")]
public class ReturnDecision : Decision
{
    public override bool Decide(StateController controller)
    {
            if (controller.isReturning)
                return true;
            else
                return false;
    }
}
