﻿using UnityEngine;

[CreateAssetMenu(menuName = "AI/Decisions/Chase")]
public class ChaseDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        float distance = Vector3.Distance(controller.player.transform.position, controller.transform.position);
        if (distance < controller.enemyStats.detectionRange && distance > controller.enemyStats.range)
            return true;
        else
            return false;
    }
}
