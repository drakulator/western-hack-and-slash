﻿using UnityEngine;

[CreateAssetMenu(menuName = "AI/Decisions/Attack")]
public class AttackDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        float distance = Vector3.Distance(controller.player.transform.position, controller.transform.position);
        if (distance <= controller.enemyStats.range)
            return true;
        else
            return false;
    }
}
