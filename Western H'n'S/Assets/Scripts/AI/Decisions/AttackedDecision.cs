﻿using UnityEngine;

[CreateAssetMenu(menuName = "AI/Decisions/Attacked")]
public class AttackedDecision : Decision
{
    public override bool Decide(StateController controller)
    {
        if (controller.enemyStats.isAttacked)
            return true;
        else
            return false;
    }
}
