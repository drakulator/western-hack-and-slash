﻿using UnityEngine;

[CreateAssetMenu(menuName = "AI/Actions/Chase")]
public class ChaseAction : AIAction
{
    public override void Act(StateController controller)
    {
        controller.agent.destination = controller.player.transform.position;
    }

    public override void OnStateEnter(StateController controller)
    {
        Debug.Log("Chasing");
        controller.animator.SetBool("Is Running", true);
        controller.animator.SetTrigger("Start Running");
        controller.returnCoroutine = controller.StartCoroutine("WaitBeforeReturn");
    }

    public override void OnStateExit(StateController controller)
    {
        Debug.Log("Stopped Chasing");
        controller.animator.SetBool("Is Running", false);
        controller.StopCoroutine(controller.returnCoroutine);
    }
}
