﻿    using UnityEngine;

[CreateAssetMenu(menuName = "AI/Actions/Wait")]
public class WaitAction : AIAction
{
    public override void Act(StateController controller)
    {
        //Do nothing and wait
    }

    public override void OnStateEnter(StateController controller)
    {
        Debug.Log("Waiting");
    }

    public override void OnStateExit(StateController controller)
    {
        Debug.Log("Stopped Waiting");
    }
}
