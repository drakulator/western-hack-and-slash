﻿using UnityEngine;

[CreateAssetMenu(menuName = "AI/Actions/Return")]
public class ReturnAction : AIAction
{
    public override void Act(StateController controller)
    {
        controller.agent.destination = controller.startPosition;
    }

    public override void OnStateEnter(StateController controller)
    {
        Debug.Log("Returning");
        controller.enemyStats.isAttacked = false;
        controller.isReturning = false;
    }

    public override void OnStateExit(StateController controller)
    {
        Debug.Log("Stopped Returning");
    }
}
