﻿using UnityEngine;
using System.Collections;

[CreateAssetMenu(menuName = "AI/Actions/Attack")]
public class AttackAction : AIAction
{
    public override void Act(StateController controller)
    {
        if (!GameConsts.isPlayerDead)
        {
            controller.Attack();

            // Rotate towards target
            Vector3 direction = (controller.player.transform.position - controller.transform.position).normalized;
            Quaternion lookRotation = Quaternion.LookRotation(new Vector3(direction.x, 0, direction.z));
            controller.transform.rotation = Quaternion.Slerp(controller.transform.rotation, lookRotation, Time.deltaTime * controller.enemyStats.angularSpeed);
        }
    }

    public override void OnStateEnter(StateController controller)
    {
        Debug.Log("Attacking");
        controller.animator.SetBool("Is Attacking", true);
        controller.agent.stoppingDistance = controller.enemyStats.range - 0.5f; //offset in case agent stopped outside of attack range
    }

    public override void OnStateExit(StateController controller)
    {
        Debug.Log("Stopped Attacking");
        controller.animator.SetBool("Is Attacking", false);
        controller.agent.stoppingDistance = controller.defaultStoppingDistance;
        if(controller.attackCoroutine != null)
            controller.StopAttacking();
    }
}
