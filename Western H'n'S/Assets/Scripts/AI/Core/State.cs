﻿using UnityEngine;

[CreateAssetMenu (menuName = "AI/State")]
public class State : ScriptableObject
{
    public AIAction[] actions;
    public Transition[] transitions;
    public Color gizmoColor;

    public void UpdateState(StateController controller)
    {
        DoActions(controller);
        CheckTransitions(controller);
    }

    public void DoActions(StateController controller)
    {
        foreach (AIAction action in actions)
        {
            action.Act(controller);
        }
    }

    private void CheckTransitions(StateController controller)
    {
        foreach (Transition transition in transitions)
        {
            if (transition.decision.Decide(controller))
            {
                controller.TransitionToState(transition.nextState);
                break;
            }
        }
    }

    public void OnStateEnter(StateController controller)
    {
        foreach (AIAction action in actions)
        {
            action.OnStateEnter(controller);
        }
    }

    public void OnStateExit(StateController controller)
    {
        foreach (AIAction action in actions)
        {
            action.OnStateExit(controller);
        }
    }
}
