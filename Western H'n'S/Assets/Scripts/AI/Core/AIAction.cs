﻿using UnityEngine;

public abstract class AIAction : ScriptableObject
{
    public abstract void Act(StateController controller);
    public abstract void OnStateEnter(StateController controller);
    public abstract void OnStateExit(StateController controller);
}
