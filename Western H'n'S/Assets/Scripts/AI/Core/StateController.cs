﻿using UnityEngine;
using UnityEngine.AI;
using System.Collections;

public class StateController : MonoBehaviour
{
    public State currentState;
    [Range(0f, 1f)]
    public float AttackAnimationTiming = 0.5f;

    [HideInInspector]
    public EnemyStatsManager enemyStats;
    [HideInInspector]
    public PlayerStatsManager playerStats;
    [HideInInspector]
    public Transform chaseTarget;
    [HideInInspector]
    public GameObject player;
    [HideInInspector]
    public NavMeshAgent agent;
    [HideInInspector]
    public Animator animator;
    [HideInInspector]
    public bool isReturning = false;
    [HideInInspector]
    public Coroutine returnCoroutine;
    [HideInInspector]
    public Vector3 startPosition;
    [HideInInspector]
    public float defaultStoppingDistance;
    [HideInInspector]
    public Coroutine attackCoroutine;

    private bool isWaiting = false;

    // Use this for initialization
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerStats = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerStatsManager>();
    }

        // Update is called once per frame
        void Update () {
        if (currentState != null)
            currentState.UpdateState(this);
    }

    void OnDrawGizmos()
    {
        if (currentState != null)
            Gizmos.color = currentState.gizmoColor;
        else
            Gizmos.color = Color.gray;
        Gizmos.DrawWireSphere(transform.position, enemyStats.detectionRange);
    }

    private void OnValidate()
    {
        enemyStats = GetComponent<EnemyStatsManager>();
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        startPosition = transform.position;
        defaultStoppingDistance = agent.stoppingDistance;
    }

    public void TransitionToState(State nextState)
    {
        if(currentState != nextState)
        {
            currentState.OnStateExit(this);
            currentState = nextState;
            currentState.OnStateEnter(this);
        }
    }

    public void Attack()
    {
        if (!isWaiting && !GameConsts.isPlayerDead)
        {
            attackCoroutine = StartCoroutine(WaitBetweenAttacks(enemyStats.attackSpeed));
        }
    }

    private IEnumerator WaitBetweenAttacks(float attackSpeed)
    {
        isWaiting = true;
        animator.SetTrigger("Attack");
        yield return new WaitForSeconds(AttackAnimationTiming / attackSpeed);
        if (enemyStats.projectile == null)
        {
            playerStats.GiveDamage(enemyStats.damageType, enemyStats.damage, enemyStats.attackEffects);
        }
        else
        {
            Shoot();
        }
        yield return new WaitForSeconds((1f - AttackAnimationTiming) / attackSpeed);
        isWaiting = false;
    }

    public void StopAttacking()
    {
        StopCoroutine(attackCoroutine);
        StartCoroutine(WaitBeforeEnablingAttack(enemyStats.attackSpeed));
    }

    public IEnumerator WaitBeforeEnablingAttack(float attackSpeed)
    {
        yield return new WaitForSeconds(0.5f / attackSpeed);
        isWaiting = false;
    }

    private IEnumerator WaitBeforeReturn()
    {
        yield return new WaitForSeconds(GameConsts.LOSE_INTEREST_TIME);
        if(Vector3.Distance(player.transform.position, transform.position) > enemyStats.detectionRange)
        {
            isReturning = true;
        }
    }

    public void Shoot()
    {
        var projectile = Instantiate(enemyStats.projectile, transform.position, transform.rotation);
        projectile.GetComponent<Projectile>().Fire(player.transform.position, enemyStats.damageType, enemyStats.damage, enemyStats.range, 1, enemyStats.attackEffects);
    }
}
